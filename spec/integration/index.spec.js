var request = require('supertest'),
    expect = require('chai').expect,
    express = require('express');

process.env.PORT = 9001;
var app = require('../../app.js'); 

describe('/', function(){

  it('shoud return login page', function(done) {
    request(app)
      .get('/')
      .expect(200)
      .end(function(err, res){
        if (err) throw err;
        done();
      });
  });
});
