#!/usr/bin/env node

var Promise = require('bluebird');

var request = Promise.promisify(require('request')),
    jsdom = require('jsdom'),
    Twit = require('twit'),
    iconv = require('iconv');

var log = require('../utils/logger');
var matcher = require('../utils/matcher');

// db settings
require('../config/db');
var mongoose = require('mongoose');
var Press = mongoose.model('Press');
var User = mongoose.model('User');

// constants
var jquerySrc = 'http://code.jquery.com/jquery-2.1.1.js';
var LIMIT_OF_DOCUMENTS = 100;

var correctTweetContainsUri = function(userId, lastCreatedAt, twit) {
  return new Promise(function(resolve, rejec) {
    twit.get('statuses/home_timeline', { count: 100 }, function(err, data, response) {
      if(err) {
        log.error('Failed to get tweets.', err);
        return resolve(null);
      }
      var presses = [];
      for(var i = 0 ; i < data.length ; i++) {
        var item = data[i];
        var uri = matcher.url.first(item.text);
        if(!uri) {
          continue;
        }
        var time = Number(new Date(item.created_at));

        if(lastCreatedAt >= time) {
          continue;
        }
        var press = new Press({
          userId: userId,
          screen_name: item.user.screen_name,
          user_name: item.user.name,
          profile_image_uri: item.user.profile_image_url,
          created_at: time,
          text: item.text,
          link: {
            url: uri
          }
        });
        presses.push(press);
      }
      log.info('Target data count: [' + presses.length + ']');
      return resolve(presses);
    });
  });
};

var convertCharset = function(response, buf) {
  var charset = null;
  var content_type = response.headers['content-type'];
  if (content_type) {
    var re = content_type.match(/\bcharset=([\w\-]+)\b/i);
    if (re) {
      charset = re[1];
    }
  }

  if (!charset) {
    var bin = buf.toString('binary');
    re = bin.match(/<meta\b[^>]*charset=([\w\-]+)/i);
    if (re) {
      charset = re[1];
    } else {
      charset = 'utf-8';
    }
  }

  switch (charset) {
  case 'ascii':
  case 'utf-8':
    return buf.toString(charset);
    break;

  default:
    var ic = new (iconv.Iconv)(charset, 'utf-8');
    var buf2 = ic.convert(buf);
    return buf2.toString('utf8');
    break;
  }
};

var fetchExtractContent = function(press) {
  return new Promise(function(resolve, reject) {
    log.info('extracting : ');

    var uri = press.link.url;

    request({uri:uri, timeout: 10000})
      .spread(function(res, body) {
        if (res.statusCode != 200) {
          log.info('Failed to get HTML content.', res.statusCode);
          return resolve(null);
        } else if(!body || body.length == 0) {
          log.info('Body is empty.');
          return resolve(null);
        }

        try {
          log.info('Converting charset start.');
          body = convertCharset(res, body);
          log.info('Converting charset completed.');
        } catch(e) {
          log.warn('Failed to convert charset.', e);
          return resolve(null);
        }

        var options = {
          features:{
            FetchExternalResources: false,
            ProcessExternalResources: false
          }
        };
        var window = jsdom.jsdom(body, options).parentWindow;
        jsdom.jQueryify(window, jquerySrc, function (window) {
          log.info('jQueryify success. [' + press.link.url + ']');
          press.link.title = window.$('title').text();
          press.link.content = window.$('p').text().substr(0, 100).replace(/\s+/g, " ");

          var img = window.$("main img");
          if(img.length < 1) {
            img = window.$("#main img");
          }
          if(img.length < 1) {
            img = window.$(".main img");
          }
          if(img.length < 1) {
            img = window.$("[role='main'] .permalink-footer  img");
          }
          if(img.length < 1) {
            img = window.$('img');
          }

          img.each(function() {
            if(this.src) {
              if(this.src.indexOf('http') > -1) {
                press.link.image_url = this.src;
                return false;
              }
            }
            return true;
          });
          log.info('insert data start. data=[' + JSON.stringify(press) + ']');
          insertData(press, function() {
            log.info('insert data completed.');
            return resolve(null);
          });
        });
      })
      .catch(function(err) {
        log.warn('Failed to connect link.', err);
        return resolve(null);
      });
  });
};

var insertData = function(press, done) {
  press.save(function(err, doc) {
    if(err) {
      log.error('Failed to insert data to db.', err);
    }
    done();
  });
};

var closeConnection = function() {
  log.info('Close connection.');
  mongoose.disconnect();
};

// Twitter configuration.
try {
  var twitter = require('../config/secret').twitter;
} catch(e) {
  twitter = 
    {
      consumerKey: process.env.CONSUMER_KEY,
      consumerSecret: process.env.CONSUMER_SECRET
    };
}

var retrieveLastCreatedAt = function(userId) {
  return new Promise(function(resolve, reject) {
    Press.find({userId: userId}, 'created_at', {limit: 1, sort: {created_at: -1}}).exec()
      .then(function(docs) {
        if(docs[0]) {
          resolve(docs[0].created_at);
        }
        resolve(0);
      });
  });
};

var deleteOldRecords = function(userId) {
  log.info('Start delete old records. userId: ' + userId);
  return Promise.resolve(Press.count({userId: userId}).exec())
    .then(function(count) {
      log.info('Exists records: ' + count);
      if(count > LIMIT_OF_DOCUMENTS) {
        // delete old records
        var oldRecordsCount = count - LIMIT_OF_DOCUMENTS;
        log.info('Records should be deleted: ' + oldRecordsCount);
        return Press.find({userId: userId}, '_id', {limit: oldRecordsCount, sort: {created_at: 1}}).exec()
          .then(function(docs) {
            docs.forEach(function(doc) {
              log.info('Remove: ' + doc._id);
              return Press.remove({_id: doc._id}).exec();
            });
          });
      }
    });
};

var fetchUser = function(user) {
  var userId = user.twitterId;
  log.info('Crawl start. user: [' + userId + ']');

  return retrieveLastCreatedAt(userId)
    .then(function(lastCreatedAt) {
      var twit = new Twit({
        consumer_key: twitter.consumerKey,
        consumer_secret: twitter.consumerSecret,
        access_token: user.accessToken,
        access_token_secret: user.accessTokenSecret
      });

      return correctTweetContainsUri(userId, lastCreatedAt, twit)
        .then(function(presses) {
          return Promise.all(presses.map(function(press) {
            return fetchExtractContent(press);
          }));
        })
        .then(function() {
          return deleteOldRecords(userId);
        });
    });
};

Promise.resolve(User.find({}).exec())
  .then(function(users) {
    return Promise.all(users.map(function(user) {
      return fetchUser(user);
    }));
  })
  .then(function() {
    log.info('Success!!!!!');
  })
  .catch(function(e) {
    log.warn('Failed!!!!! error [' + e + ']');
  })
  .finally(function() {
    closeConnection();
  });

