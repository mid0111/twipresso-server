var app = require('../app');
var session = require('express-session');
var passport = require('passport');
var TwitterStrategy = require('passport-twitter').Strategy;
var log = require('../utils/logger');

try {
  var sessionSecret = require('../config/secret').session.secret;
} catch(e) {
  sessionSecret = process.env.SESSION_SECRET;
}


try {
  var twitterConfig = require('../config/secret').twitter;
} catch(e) {
  twitterConfig = 
    {
      consumerKey: process.env.CONSUMER_KEY,
      consumerSecret: process.env.CONSUMER_SECRET
    };
}

var mongoose = require('mongoose');
var User = mongoose.model('User');

app.use(session({
  secret: sessionSecret,
  resave: true,
  saveUninitialized: true,
  cookie : {
    maxAge : 604800 // one week
  }
}));
app.use(passport.initialize());
app.use(passport.session());

var options = {
  consumerKey: twitterConfig.consumerKey,
  consumerSecret: twitterConfig.consumerSecret,
  callbackURL: "/api/auth/callback"
};

passport.use(new TwitterStrategy(options, function(token, tokenSecret, profile, done) {
  User.findOne({twitterId: profile.id}, function(err, doc) {
    if(err) {
      log.error('Failed to find user data. [ ' + err + ']');
      return done(err);
    }
    var user = new User({
      twitterId: profile.id,
      name: profile.displayName,
      accessToken: token,
      accessTokenSecret: tokenSecret
    });
    if(doc) {
      log.info('The user is already registered. [' + profile.id + ']');
      return done(null, user);
    }

    // Regist new user
    user.save(function(err) {
      if(err) {
        log.error('Failed to save user data. [' + err + ']');
      }
    });
    return done(null, user);
  });
}));

passport.serializeUser(function(user, done){
  done(null, user);
});

passport.deserializeUser(function(user, done){
  done(null, user);
});

