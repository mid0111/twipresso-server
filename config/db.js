var mongoose = require('mongoose');

var uri = 'mongodb://192.168.59.103:27017/twipresso';
if(process.env.MONGOHQ_URL) {
  uri = process.env.MONGOHQ_URL;
} else if(process.env.NODE_ENV === 'development') {
  uri += '-dev';
}
mongoose.connect(uri);

// Models
require('../models/user');
require('../models/press');

