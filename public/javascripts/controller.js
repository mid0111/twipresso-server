angular.module('twiPressoApp', [])
  .controller('PressController', ['$scope', '$http', function($scope, $http) {
    $http.get('/api/press').success(function(data) {
      $scope.presses = data;
    });
  }]);
