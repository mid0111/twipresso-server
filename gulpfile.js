var gulp = require('gulp');
var uglify = require('gulp-uglify');
var mocha = require('gulp-mocha');
var bower = require('main-bower-files');
var nodemon = require('gulp-nodemon');
var livereload = require('gulp-livereload');

gulp.task('bower', function() {
  return gulp.src(bower())
    .pipe(uglify())
    .pipe(gulp.dest('public/libs'));
});

gulp.task('test', function() {
  return gulp.src('./spec/**/*.js')
    .pipe(mocha())
    .once('end', function () {
      process.exit();
    });
});

gulp.task('serve', function () {
  livereload.listen();

  // client
  gulp.watch(['views/**', 'public/**'])
    .on('change', livereload.changed);

  nodemon({
    script: './bin/www',
    ext: 'js',
    ignore: ['ignored.js', 'node_modules', 'bower_components'] })
//    .on('change', ['lint'])
    .on('start', function () {
      console.log('restarted!');
      livereload.changed();
    });
});

gulp.task('package', ['bower']);

gulp.task('default', ['bower', 'serve']);
