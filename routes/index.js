var express = require('express');
var router = express.Router();
var matcher = require('../utils/matcher');


/* GET home page. */
router.get('/', function(req, res) {
  if(!req.user && !req.cookies.userid) {
    // call endpoint for login page.
    res.render('login');
    return;
  }
  res.render('presses');
  return;
});

module.exports = router;
