var express = require('express');
var router = express.Router();
var moment = require('moment');
var log = require('../utils/logger');

var mongoose = require('mongoose');
var Press = mongoose.model('Press');


moment.locale('ja', {
    relativeTime : {
        future: "in %s",
        past:   "%s ago",
        s:  "now",
        m:  "1min",
        mm: "%dmin",
        h:  "1h",
        hh: "%dh",
        d:  "1day",
        dd: "%ddays",
        M:  "1month",
        MM: "%dmonths",
        y:  "1year",
        yy: "%dyears"
    }
});

/* GET press list. */
router.get('/', function(req, res) {

  var userId = req.user ? req.user.twitterId : req.cookies.userid;

  // Retrieve tweets contains link url.
  Press.find({userId: userId}, null, {limit: 99, sort: {created_at: -1}}, function(err, docs) {
    if(err) {
      log.error('Failed to load press lists.', err);
      res.render('error', {
        message:err.message,
        error: {}
      });
    }
    var presses = new Array();
    for(var i = 0 ; i < docs.length ; i++) {
      var doc = docs[i];
      if(doc.link.content && doc.link.title) {
        var fromNow = moment(Number(doc.created_at)).fromNow(true);
        doc.created_at = fromNow;
        presses.push(doc);
      }
    }
    res.send(presses);
  });
});

module.exports = router;
