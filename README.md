TwiPresso-server  [ ![Codeship Status for mid0111/Twipresso-server](https://www.codeship.io/projects/062b6d30-35be-0132-8ee7-2658e26501e3/status)](https://www.codeship.io/projects/41173)
================

http://twipresso.herokuapp.com/

## Development environment settings.

### Requirements

* node >= v0.10.32
* npm >= 2.1.4
* mongo >= 2.6.5

### Installation

```bash
$ npm install
$ bower install
$ cp config/sample/secret.js config/secret.js
$ vi config/secret.js  # edit your twitter settings.
```

### Start server

```bash
$ gulp
```

Open browser and regist your twitter acount to server.  
http://localhost:3000

### Scrape web site based on your twitter home time line including url string.

```bash
node bin/crawler.js
```

### Running mongo on docker

If you want to run mongo on docker not your local machine, try below.
```bash
$ boot2docker start
$ docker run -d -p 27017:27017 --name mongodb dockerfile/mongodb
$ docker run -it --rm --link mongodb:mongodb dockerfile/mongodb bash -c 'mongo --host mongodb'
```
