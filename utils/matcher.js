var regexp_url = /((h?)(ttps?:\/\/[a-zA-Z0-9.\-_@:/~?%&;=+#',()*!]+))/g; // ']))/;

module.exports = {
  url : {
    first : function(doc) {
      var texts = doc.match(regexp_url);
      if(texts) {
        return texts[0];
      }
      return null;
    }
  }
};
