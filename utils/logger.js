module.exports = {
  info : function(message, reason) {
    var line = '[INFO ]' + message + (reason ? 'reason: ['+ reason +']' : '');
    console.log(line);
  },

  warn : function(message, reason) {
    var line = '[WARN ]' + message + (reason ? 'reason: ['+ reason +']' : '');
    console.log(line);
  },

  error : function(message, reason) {
    var line = '[ERROR]' + message + (reason ? 'reason: ['+ reason +']' : '');
    console.error(line);
  }
};
