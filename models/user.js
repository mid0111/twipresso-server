var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var User = new Schema({
  id: ObjectId,
  twitterId: {type: String, required: true, unique: true},
  name: {type: String, require: true},
  accessToken: {type: String, require: true},
  accessTokenSecret: {type: String, require: true}
});

mongoose.model('User', User);
