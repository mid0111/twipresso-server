var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Press = new Schema({
  id: ObjectId,
  userId: {type: String, required: true},
  screen_name: String,
  user_name: String,
  profile_image_uri: String,
  created_at: String,
  text: String,
  link: {
    url: String,
    title: String,
    content: String,
    image_url: String
  }
});

mongoose.model('Press', Press);
